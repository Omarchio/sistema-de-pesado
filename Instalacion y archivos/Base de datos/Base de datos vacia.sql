-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: pesaje
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articulos`
--

DROP TABLE IF EXISTS `articulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articulos` (
  `idarticulos` int(3) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `pvd` decimal(7,3) DEFAULT NULL,
  `fechacreacion` datetime DEFAULT NULL,
  PRIMARY KEY (`idarticulos`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cuentasguardadas`
--

DROP TABLE IF EXISTS `cuentasguardadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentasguardadas` (
  `idcuentasguardadas` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idcuentasguardadas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lotes`
--

DROP TABLE IF EXISTS `lotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lotes` (
  `idlotes` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(4) DEFAULT NULL,
  `lote` varchar(45) DEFAULT NULL,
  `articulos_idarticulos` int(11) NOT NULL,
  `fechalote` datetime DEFAULT NULL,
  `fechacreacion` datetime DEFAULT NULL,
  `rutas_idrutas` int(11) NOT NULL,
  `tamanos_idtamano` int(11) NOT NULL,
  `salieron1entraron2` int(1) DEFAULT NULL,
  PRIMARY KEY (`idlotes`,`rutas_idrutas`,`tamanos_idtamano`,`articulos_idarticulos`),
  KEY `fk_lotes_rutas1_idx` (`rutas_idrutas`),
  KEY `fk_lotes_unidades1_idx` (`tamanos_idtamano`),
  KEY `fk_lotes_articulos1_idx` (`articulos_idarticulos`),
  CONSTRAINT `fk_lotes_idarticulos` FOREIGN KEY (`articulos_idarticulos`) REFERENCES `articulos` (`idarticulos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_lotes_rutas` FOREIGN KEY (`rutas_idrutas`) REFERENCES `rutas` (`idrutas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_lotes_tamanos` FOREIGN KEY (`tamanos_idtamano`) REFERENCES `tamanos` (`idtamano`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=348 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pesadas`
--

DROP TABLE IF EXISTS `pesadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pesadas` (
  `idpesadas` int(11) NOT NULL AUTO_INCREMENT,
  `kilos` decimal(7,3) DEFAULT NULL,
  `kilos_cajas` decimal(7,3) NOT NULL,
  `fechacreacion` datetime NOT NULL,
  `salieron1entraron2` int(1) DEFAULT NULL,
  `rutas_idrutas` int(11) NOT NULL,
  `comentarios` varchar(45) DEFAULT NULL,
  `articulos_idarticulos` int(3) NOT NULL,
  PRIMARY KEY (`idpesadas`,`rutas_idrutas`,`articulos_idarticulos`),
  KEY `id` (`idpesadas`),
  KEY `fk_pesadas_rutas1_idx` (`rutas_idrutas`),
  KEY `fk_pesadas_articulos1_idx` (`articulos_idarticulos`),
  CONSTRAINT `fk_pesadas_articulos1` FOREIGN KEY (`articulos_idarticulos`) REFERENCES `articulos` (`idarticulos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pesadas_rutas1` FOREIGN KEY (`rutas_idrutas`) REFERENCES `rutas` (`idrutas`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=62058 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rutas`
--

DROP TABLE IF EXISTS `rutas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rutas` (
  `idrutas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(10) NOT NULL,
  `zona` varchar(20) DEFAULT NULL,
  `repartidor` varchar(20) NOT NULL,
  `fechacreacion` datetime DEFAULT NULL,
  PRIMARY KEY (`idrutas`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tamanos`
--

DROP TABLE IF EXISTS `tamanos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tamanos` (
  `idtamano` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `comentarios` varchar(45) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`idtamano`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fechacreacion` datetime DEFAULT NULL,
  PRIMARY KEY (`idusuarios`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-25 13:59:19
