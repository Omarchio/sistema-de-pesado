/*
 * Copyright (C) 2015 robertchio@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Pesajevistas;
import ConexionMysql.conexion;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import utilidades.tecladonumerico;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import jssc.SerialPortException;

/**
 *
 * @author Roberto Jimenez:
 * Robertchio@gmail.com
 */
public class llegada extends javax.swing.JFrame implements Runnable{
    private static final long serialVersionUID = 1L;
    private String fechayhoraactual = "";            // yyyy-MM-dd HH:mm:ss
    private String nombrerutatemporal = "";          // Nombre de la ruta para hacer las consultas e inserts mysql
    private String nombrearticulotemporal = "";      // Lo mismo que la linea de arriba
    private String nombreTamanoTemporal = "";
    private String idrutatemporal = "";              // Mas de lo mismo
    private String idarticulotemporal = "";          // Ajumm
    private String idTamanoTemporal = "";            // ...
    private String diadehoy = "";                    // Por si lo quiero mostrar en la interfaz.
    private static final int salio1entro2 = 2;       // colocamos 1 si sale el reparto y 2 si llega, es para guardar este digito en la base de datos.
    private static String haydatosenlafecha = "no";  // es para controlar cuando se pulsa guardar lote que hayan introducido una fecha de caducidad.
    private static String fechalotetemporal = "";
  
   
    public  llegada() {
        initComponents();
        Toolkit toolkit = getToolkit();
        Dimension size = toolkit.getScreenSize();
        setLocation(size.width/2 - getWidth()/2,
        size.height/2 - getHeight()/2);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH); 
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/icono_app.png")).getImage());            
    }
    private void leerrutasajtable(){
                 try {
                  String consulta = "Select rutas.nombre from rutas";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                ResultSetMetaData meta = rs.getMetaData();
                int numerocolumnas = meta.getColumnCount();
                DefaultTableModel modelo = new DefaultTableModel();
                this.jTablerutas.setModel(modelo);
          for (int x=1; x<=numerocolumnas; x++){
             modelo.addColumn(meta.getColumnLabel(x));          
          }
          while (rs.next()){
                    Object [] fila = new Object [numerocolumnas];
                for(int y = 0; y<numerocolumnas; y++){
                    fila [y]= rs.getObject(y+1);
                    }
                 modelo.addRow(fila);
           }
           
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    private void leerarticulosjtable(){
            try {
                String consulta = "Select articulos.nombre from articulos";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                ResultSetMetaData meta = rs.getMetaData();
                int numerocolumnas = meta.getColumnCount();
                DefaultTableModel modelo = new DefaultTableModel();
                this.jTablearticulos.setModel(modelo);
          for (int x=1; x<=numerocolumnas; x++){
             modelo.addColumn(meta.getColumnLabel(x));          
          }
          while (rs.next()){
                    Object [] fila = new Object [numerocolumnas];
                for(int y = 0; y<numerocolumnas; y++){
                    fila [y]= rs.getObject(y+1);
                    }
                 modelo.addRow(fila);
           }
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    private void conseguirrutaseleccionada(){
            int row = jTablerutas.getSelectedRow();
            String seleccion = (jTablerutas.getModel().getValueAt(row, 0).toString());
            nombrerutatemporal = seleccion;
            try {
                  String consulta = "Select rutas.idrutas from pesaje.rutas where rutas.nombre ='"+seleccion+"'";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                // lo siguiente es para pasar el unico registro obtenido del resultset a string pero hace falta siempre el label de la columna.
                while (rs.next()) {                    
            idrutatemporal = rs.getString("idrutas");  
                }
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    private void ConseguirIdArticuloSeleccionado(){        
            int row = jTablearticulos.getSelectedRow();
            String seleccion = (jTablearticulos.getModel().getValueAt(row, 0).toString());
            nombrearticulotemporal = seleccion;
            try {
                ConexionMysql.conexion ca = new ConexionMysql.conexion();
                  String consulta = "Select articulos.idarticulos from pesaje.articulos where articulos.nombre = '"+seleccion+"'";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                // lo siguiente es para pasar el unico registro obtenido del resultset a string pero hace falta siempre el label de la columna.
                while (rs.next()) {                    
                           idarticulotemporal = rs.getString("idarticulos");
                }
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    private void leerunidadesjtable(){
            try {
                  String consulta = "Select tamanos.nombre from tamanos";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                ResultSetMetaData meta = rs.getMetaData();
                int numerocolumnas = meta.getColumnCount();
                DefaultTableModel modelo = new DefaultTableModel();
                this.jTableTamano.setModel(modelo);          
                for (int x=1; x<=numerocolumnas; x++){
             modelo.addColumn(meta.getColumnLabel(x));          
          }
          while (rs.next()){
                    Object [] fila = new Object [numerocolumnas];
                for(int y = 0; y<numerocolumnas; y++){
                    fila [y]= rs.getObject(y+1);
                    }
                 modelo.addRow(fila);
           }
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    private void leerlistaunidadesylotesdelarutaseleccionada(){
         try {
                String consulta;
                        consulta = "Select sum(cantidad) as Unidades, lotes.lote, lotes.fechalote, articulos.nombre, tamanos.nombre "+
                                   "from lotes inner join pesaje.articulos on lotes.articulos_idarticulos = articulos.idarticulos "+
                                   "inner join pesaje.tamanos on lotes.tamanos_idtamano = tamanos.idtamano "+
                                   "inner join pesaje.rutas on rutas.nombre = '"+nombrerutatemporal+"' "+
                                   "and lotes.rutas_idrutas = rutas.idrutas and DATE(lotes.fechacreacion) = CURDATE() and lotes.salieron1entraron2 = '"+salio1entro2+"' group by 2,4,5"; //curdate es fecha actual.
                        // and pesadas.salieron1entraron2 = '"+salio1entro2+"' group by 2
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                ResultSetMetaData meta = rs.getMetaData();
                int numerocolumnas = meta.getColumnCount();
                DefaultTableModel modelo = new DefaultTableModel();
                jTableunidadesylotes.setModel(modelo);
         for (int x=1; x<=numerocolumnas; x++){
             modelo.addColumn(meta.getColumnLabel(x));          
          }
          while (rs.next()){
                    Object [] fila = new Object [numerocolumnas];
                for(int y = 0; y<numerocolumnas; y++){
                    fila [y]= rs.getObject(y+1);
                    }
                 modelo.addRow(fila);
           }   
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    private void conseguirdiadehoy(){
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, 0);
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                    String choy = format1.format(cal.getTime());
                    diadehoy = choy;
    }
    private void conseguirfechayhoraactual(){
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, 0);
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String ahoramismo = format1.format(cal.getTime());
                    fechayhoraactual = ahoramismo;   
    } 
    private void leerlistadepesadasjtable(){ // Con esto relleno el jtable  con la lista de pesadas con la ruta seleccionada, fecha actual                   
            try {
         
                String consulta;
                        consulta = "select idpesadas as id, kilos,kilos_cajas, articulos.nombre as articulo, pesadas.fechacreacion, rutas.nombre as Ruta \n" +
                                   "from pesadas \n" +
                                   "inner join rutas on rutas.nombre = '"+nombrerutatemporal+"' \n" +
                                   "inner join articulos on articulos.idarticulos = pesadas.articulos_idarticulos \n" +
                                   "and pesadas.rutas_idrutas = rutas.idrutas and DATE(pesadas.fechacreacion) = CURDATE() and pesadas.salieron1entraron2 = '"+salio1entro2+"' order by id desc"; // group by 2"; // curdate es fecha actual.
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                ResultSetMetaData meta = rs.getMetaData();
                int numerocolumnas = meta.getColumnCount();
                DefaultTableModel modelo = new DefaultTableModel();
                this.jTablelistadepesadas.setModel(modelo);
         for (int x=1; x<=numerocolumnas; x++){
             modelo.addColumn(meta.getColumnLabel(x));          
          }
          while (rs.next()){
                    Object [] fila = new Object [numerocolumnas];
                for(int y = 0; y<numerocolumnas; y++){
                    fila [y]= rs.getObject(y+1);
                    }
                 modelo.addRow(fila);
           }           
        } catch (ClassNotFoundException | SQLException e) {
        }
    
    }
    private void guardarpesadas(){
           if (nombrerutatemporal.isEmpty()){
               JOptionPane.showMessageDialog(null, "Seleccione una ruta","Error", JOptionPane.ERROR_MESSAGE);
           }else if(nombrearticulotemporal.isEmpty()){
               JOptionPane.showMessageDialog(null, "Seleccione un articulo","Error", JOptionPane.ERROR_MESSAGE);
           }else if("".equals(jTextFieldPesallegada.getText())){
               JOptionPane.showMessageDialog(null, "Introduzca el peso o pulse leer pesa","Error", JOptionPane.ERROR_MESSAGE);
           }else if("".equals(jTextFielCajasllegadas.getText())){
               JOptionPane.showMessageDialog(null, "Introduzca las cajas que fueron utilizadas","Error", JOptionPane.ERROR_MESSAGE);
           }else{    
               try {
                    double pesodelpanelpesaParseDouble = Double.parseDouble(jTextFieldPesallegada.getText());
                    double cajasmetidasParseDouble = Double.parseDouble(jTextFielCajasllegadas.getText());
                    double kilosencajas = cajasmetidasParseDouble * 2.7;
                    double kilossincajaaguardar = (pesodelpanelpesaParseDouble - kilosencajas);
                    conexion con = new ConexionMysql.conexion();
                    String insertar;
                           insertar = "insert into pesaje.pesadas (idpesadas, kilos, kilos_cajas, fechacreacion, salieron1entraron2, \n"
                           +"rutas_idrutas, comentarios, articulos_idarticulos) \n"
                           +"values (null, "+kilossincajaaguardar+", "+kilosencajas+" , '"+fechayhoraactual+"', "+salio1entro2+", "+idrutatemporal+", null,"+idarticulotemporal+")";
                         con.consultar(insertar);
                        JOptionPane.showMessageDialog(null, "Datos guardados");          
                        limpiarcampos();                        
                    } catch (ClassNotFoundException | HeadlessException e) {
                        JOptionPane.showInternalMessageDialog(null, "Error al guardar los datos "+e,"Error", JOptionPane.ERROR_MESSAGE);
                  }
            }
    }
    private void ConseguirIdTamanoTemporal(){
            int row = jTableTamano.getSelectedRow();
            String seleccion = (jTableTamano.getModel().getValueAt(row, 0).toString());
            nombreTamanoTemporal = seleccion;
            try {
                ConexionMysql.conexion ca = new ConexionMysql.conexion();
                  String consulta = "Select tamanos.idtamano from pesaje.tamanos where tamanos.nombre = '"+seleccion+"'";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                // lo siguiente es para pasar el unico registro obtenido del resultset a string pero hace falta siempre el label de la columna.
                while (rs.next()) {                    
                           idTamanoTemporal = rs.getString("idtamano");
                }
        } catch (ClassNotFoundException | SQLException e) {
        }  
    }
    private void guardarloteenlabasededatos(){
           try {
                conexion con = new ConexionMysql.conexion();
                String insertar;
                insertar = "insert into pesaje.lotes (idlotes, cantidad, lote, articulos_idarticulos, fechalote, fechacreacion, rutas_idrutas, "
                          +"tamanos_idtamano, salieron1entraron2) "
                          +"values (null, '"+jTextFielCantidadLlegada.getText()+"', "+jTextFielLoteLlegada.getText()+", '"+idarticulotemporal+"', '"+fechalotetemporal+"', '\n"
                          +fechayhoraactual+"', '"+idrutatemporal+"', '"
                          +idTamanoTemporal+"', "+salio1entro2+")";
                con.consultar(insertar);
                limpiarcampos();
                JOptionPane.showMessageDialog(null, "Datos guardados");
            }catch (ClassNotFoundException | HeadlessException e) {
                                    JOptionPane.showMessageDialog(null, "Los Datos no se han podido guardar"+e,"Error", JOptionPane.ERROR_MESSAGE);
        } 
    }
    private void limpiarcampos(){
           jTablerutas.clearSelection();
           jTablearticulos.clearSelection();                       
           jTablelistadepesadas.setModel(new DefaultTableModel());
           jTableunidadesylotes.setModel(new DefaultTableModel());
           jTableTamano.clearSelection();
           nombrerutatemporal = "";
           nombrearticulotemporal = "";
           idrutatemporal = "";
           idTamanoTemporal = "";
           haydatosenlafecha = "no";
           fechalotetemporal = "";
           jTextFieldPesallegada.setText("");
           btnCaducidad.setDate(null);  // vacio los datos del jcalendarchooser.    
           jTextFielCajasllegadas.setText("");
           jTextFielCantidadLlegada.setText("");
           jTextFielLoteLlegada.setText("");
           
    }
    public  void IrMenuPrincipal(){  //dialogo en el que preguntamos si desea salir de la aplicación o no.
        Object [] opciones ={"Aceptar","Cancelar"};
        int eleccion = JOptionPane.showOptionDialog(rootPane,"¿En realidad desea regresar al menu principal?","Salir al menu principal",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE,null,opciones,"Aceptar");
        if (eleccion == JOptionPane.YES_OPTION)
        {
            Pesajevistas.eleccion aaa = new eleccion();
        aaa.setVisible(true);
        dispose();
        }else{
    }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonguardarpesada = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabelpesadasbasededatos1 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTablearticulos = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTablerutas = new javax.swing.JTable();
        jLabelpesadasbasededatos2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabelpesadasbasededatos3 = new javax.swing.JLabel();
        jTextFielCantidadLlegada = new javax.swing.JTextField();
        jTextFielLoteLlegada = new javax.swing.JTextField();
        jLabelUnidades = new javax.swing.JLabel();
        jLabelLote = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTableTamano = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabelcaducidad = new javax.swing.JLabel();
        jButtonGuardarLotes = new javax.swing.JButton();
        btnCaducidad = new com.toedter.calendar.JDateChooser();
        jButtonleerpesa = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextFielCajasllegadas = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        btnsalir = new javax.swing.JButton();
        jTextFieldPesallegada = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTablelistadepesadas = new javax.swing.JTable();
        jScrollPane10 = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableunidadesylotes = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Llegada de repartidores");
        setLocationByPlatform(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jButtonguardarpesada.setBackground(new java.awt.Color(201, 109, 0));
        jButtonguardarpesada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/document-save-all.png"))); // NOI18N
        jButtonguardarpesada.setText("Guardar Peso");
        jButtonguardarpesada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonguardarpesadaActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Rutas");

        jLabelpesadasbasededatos1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabelpesadasbasededatos1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelpesadasbasededatos1.setText("Lista de pesadas de la ruta:");

        jTablearticulos.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jTablearticulos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Title 1"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTablearticulos.setColumnSelectionAllowed(true);
        jTablearticulos.setRowHeight(40);
        jTablearticulos.setRowMargin(2);
        jTablearticulos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTablearticulos.getTableHeader().setReorderingAllowed(false);
        jTablearticulos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTablearticulosMouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(jTablearticulos);
        jTablearticulos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jScrollPane5.setViewportView(jScrollPane3);

        jTablerutas.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jTablerutas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Title 1"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTablerutas.setColumnSelectionAllowed(true);
        jTablerutas.setRowHeight(40);
        jTablerutas.setRowMargin(2);
        jTablerutas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTablerutas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTablerutasMouseReleased(evt);
            }
        });
        jScrollPane4.setViewportView(jTablerutas);
        jTablerutas.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jScrollPane6.setViewportView(jScrollPane4);

        jLabelpesadasbasededatos2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabelpesadasbasededatos2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelpesadasbasededatos2.setText("Lista de unidades y lotes guardados de la ruta seleccionada:");

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Lectura de Pesa");

        jLabelpesadasbasededatos3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabelpesadasbasededatos3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelpesadasbasededatos3.setText("Seleccione tamaño:");

        jTextFielCantidadLlegada.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jTextFielCantidadLlegada.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFielCantidadLlegada.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jTextFielCantidadLlegada.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextFielCantidadLlegadaMouseClicked(evt);
            }
        });

        jTextFielLoteLlegada.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jTextFielLoteLlegada.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFielLoteLlegada.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextFielLoteLlegadaMouseClicked(evt);
            }
        });

        jLabelUnidades.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabelUnidades.setText("Cantidad:");

        jLabelLote.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabelLote.setText("Lote:");

        jTableTamano.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jTableTamano.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null}
            },
            new String [] {
                "Title 1"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableTamano.setRowHeight(40);
        jTableTamano.setRowMargin(2);
        jTableTamano.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTableTamano.getTableHeader().setReorderingAllowed(false);
        jTableTamano.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableTamanoMouseClicked(evt);
            }
        });
        jScrollPane7.setViewportView(jTableTamano);

        jScrollPane9.setViewportView(jScrollPane7);

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Articulo");

        jLabelcaducidad.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabelcaducidad.setText("Caducidad");

        jButtonGuardarLotes.setBackground(new java.awt.Color(201, 109, 0));
        jButtonGuardarLotes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/document-save-all.png"))); // NOI18N
        jButtonGuardarLotes.setText("Guardar lotes:");
        jButtonGuardarLotes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGuardarLotesActionPerformed(evt);
            }
        });

        jButtonleerpesa.setBackground(new java.awt.Color(201, 109, 0));
        jButtonleerpesa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pesa.png"))); // NOI18N
        jButtonleerpesa.setText("Leer pesa");
        jButtonleerpesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonleerpesaActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Numero de cajas");

        jTextFielCajasllegadas.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jTextFielCajasllegadas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFielCajasllegadas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextFielCajasllegadasMouseClicked(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(201, 109, 0));

        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/22x22/actions/go-previous.png"))); // NOI18N
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnsalir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTextFieldPesallegada.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jTextFieldPesallegada.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldPesallegada.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextFieldPesallegadaMouseClicked(evt);
            }
        });

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconollegada.png"))); // NOI18N

        jTablelistadepesadas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTablelistadepesadas.setRowHeight(30);
        jTablelistadepesadas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTablelistadepesadasMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(jTablelistadepesadas);

        jScrollPane8.setViewportView(jScrollPane2);

        jTableunidadesylotes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTableunidadesylotes);

        jScrollPane10.setViewportView(jScrollPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelpesadasbasededatos1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 560, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
                            .addComponent(jLabelpesadasbasededatos2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jButtonleerpesa, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButtonguardarpesada, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFielCajasllegadas, javax.swing.GroupLayout.Alignment.LEADING)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jTextFieldPesallegada, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                            .addComponent(jLabelpesadasbasededatos3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTextFielCantidadLlegada)
                                .addComponent(jTextFielLoteLlegada)
                                .addComponent(btnCaducidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonGuardarLotes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelUnidades)
                                    .addComponent(jLabelLote)
                                    .addComponent(jLabelcaducidad))
                                .addGap(81, 81, 81))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextFieldPesallegada, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonleerpesa)
                                .addGap(40, 40, 40)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFielCajasllegadas, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonguardarpesada))
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 456, Short.MAX_VALUE)
                            .addComponent(jScrollPane6)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelUnidades)
                            .addComponent(jLabelpesadasbasededatos3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextFielCantidadLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabelLote)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFielLoteLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)
                                .addComponent(jLabelcaducidad)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCaducidad, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(7, 7, 7)
                                .addComponent(jButtonGuardarLotes)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane9))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelpesadasbasededatos1)
                    .addComponent(jLabelpesadasbasededatos2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
           IrMenuPrincipal();
    }//GEN-LAST:event_btnsalirActionPerformed
    private void jButtonguardarpesadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonguardarpesadaActionPerformed
                guardarpesadas();   
    }//GEN-LAST:event_jButtonguardarpesadaActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        leerarticulosjtable();
        leerrutasajtable();
        leerunidadesjtable();
        conseguirdiadehoy();
        conseguirfechayhoraactual();
    }//GEN-LAST:event_formWindowOpened
        
    private void jTablerutasMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablerutasMouseReleased
                    conseguirrutaseleccionada();
                    leerlistadepesadasjtable();
                    leerlistaunidadesylotesdelarutaseleccionada();
                    
    }//GEN-LAST:event_jTablerutasMouseReleased
    private void jTextFielCantidadLlegadaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFielCantidadLlegadaMouseClicked
       utilidades.tecladonumerico.setdedondefuellamadoelteclado(5);
       utilidades.tecladonumerico tec = new tecladonumerico();
               tec.setVisible(true);
    }//GEN-LAST:event_jTextFielCantidadLlegadaMouseClicked
    private void jTextFielLoteLlegadaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFielLoteLlegadaMouseClicked
       utilidades.tecladonumerico.setdedondefuellamadoelteclado(6);
       utilidades.tecladonumerico tec = new tecladonumerico();
               tec.setVisible(true);
    }//GEN-LAST:event_jTextFielLoteLlegadaMouseClicked
    private void jButtonGuardarLotesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGuardarLotesActionPerformed
            Date jdate = btnCaducidad.getDate();
            SimpleDateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd");
            if (jdate == null) {
                haydatosenlafecha = "no";
                JOptionPane.showMessageDialog(null, "Seleccione una fecha de caducidad","Error", JOptionPane.ERROR_MESSAGE);
            }else{
                String fechaformateada = formatofecha.format(jdate);
                fechalotetemporal = fechaformateada; 
                haydatosenlafecha ="si";
                    if (idrutatemporal.equals("")){
                            JOptionPane.showMessageDialog(null, "Seleccione una Ruta","Error", JOptionPane.ERROR_MESSAGE); 
                    }else if (idarticulotemporal.equals("")){
                            JOptionPane.showMessageDialog(null, "Seleccione una Articulo","Error", JOptionPane.ERROR_MESSAGE);          
                    }else if (idTamanoTemporal.equals("")){
                            JOptionPane.showMessageDialog(null, "Seleccione un tamaño","Error", JOptionPane.ERROR_MESSAGE);          
                    }else if ("".equals(jTextFielCantidadLlegada.getText())){
                            JOptionPane.showMessageDialog(null, "Introduzca la Cantidad","Error", JOptionPane.ERROR_MESSAGE);          
                    }else if ("".equals(jTextFielLoteLlegada.getText())){
                            JOptionPane.showMessageDialog(null, "Introduzca el lote","Error", JOptionPane.ERROR_MESSAGE);          
                    }else if (haydatosenlafecha.equals("no")) {
                                JOptionPane.showMessageDialog(null, fechaformateada);
                            JOptionPane.showMessageDialog(null, "Seleccione una fecha de caducidad antes de guardar los datos de caducidad","Error", JOptionPane.ERROR_MESSAGE);
                    }else{
                             guardarloteenlabasededatos();
                    }
            }
    }//GEN-LAST:event_jButtonGuardarLotesActionPerformed
    private void jTableTamanoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableTamanoMouseClicked
            int row = jTableTamano.getSelectedRow();
            String seleccion = (jTableTamano.getModel().getValueAt(row, 0).toString());
            nombreTamanoTemporal = seleccion;
            ConseguirIdTamanoTemporal();
    }//GEN-LAST:event_jTableTamanoMouseClicked
    private void jTablearticulosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablearticulosMouseReleased
            int row = jTablearticulos.getSelectedRow();
            String seleccion = (jTablearticulos.getModel().getValueAt(row, 0).toString());
            nombrearticulotemporal = seleccion;
            ConseguirIdArticuloSeleccionado();
    }//GEN-LAST:event_jTablearticulosMouseReleased

    private void jButtonleerpesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonleerpesaActionPerformed
            utilidades.lecturapuertoserie.setLeerpesasalida1entrada2(2);
        try {
            utilidades.lecturapuertoserie.leerpuerto();            
        } catch (SerialPortException ex) {
            Logger.getLogger(llegada.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }//GEN-LAST:event_jButtonleerpesaActionPerformed

    private void jTextFieldPesallegadaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldPesallegadaMouseClicked
       utilidades.tecladonumerico.setdedondefuellamadoelteclado(7); //llamo al tecladonumerico emergente y asigno un 7  y saber desde donde fue llamado.
       utilidades.tecladonumerico tec = new tecladonumerico();
       tec.setVisible(true);
    }//GEN-LAST:event_jTextFieldPesallegadaMouseClicked

    private void jTextFielCajasllegadasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFielCajasllegadasMouseClicked
       utilidades.tecladonumerico.setdedondefuellamadoelteclado(8); //llamo al tecladonumerico emergente y asigno un 8 y saber desde donde fue llamado. Cajas en llegadas
       utilidades.tecladonumerico tec = new tecladonumerico();
       tec.setVisible(true);
    }//GEN-LAST:event_jTextFielCajasllegadasMouseClicked

    private void jTablelistadepesadasMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablelistadepesadasMouseReleased
        int filaseleccionada = jTablelistadepesadas.getSelectedRow();
        String aaa = jTablelistadepesadas.getValueAt(filaseleccionada, 0).toString();
        ediciondepesadaguardada editar = new ediciondepesadaguardada();
        editar.setIdpesadatemporal(aaa);
        editar.setVisible(true);
        limpiarcampos();
    }//GEN-LAST:event_jTablelistadepesadasMouseReleased

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        IrMenuPrincipal();
    }//GEN-LAST:event_formWindowClosing
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(llegada.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new llegada().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser btnCaducidad;
    private javax.swing.JButton btnsalir;
    private javax.swing.JButton jButtonGuardarLotes;
    private javax.swing.JButton jButtonguardarpesada;
    private javax.swing.JButton jButtonleerpesa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabelLote;
    private javax.swing.JLabel jLabelUnidades;
    private javax.swing.JLabel jLabelcaducidad;
    private javax.swing.JLabel jLabelpesadasbasededatos1;
    private javax.swing.JLabel jLabelpesadasbasededatos2;
    private javax.swing.JLabel jLabelpesadasbasededatos3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTable jTableTamano;
    private javax.swing.JTable jTablearticulos;
    private javax.swing.JTable jTablelistadepesadas;
    private javax.swing.JTable jTablerutas;
    private javax.swing.JTable jTableunidadesylotes;
    public static javax.swing.JTextField jTextFielCajasllegadas;
    public static javax.swing.JTextField jTextFielCantidadLlegada;
    public static javax.swing.JTextField jTextFielLoteLlegada;
    public static javax.swing.JTextField jTextFieldPesallegada;
    // End of variables declaration//GEN-END:variables

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
