/*
 * Copyright (C) 2015 robertchio@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Pesajevistas;
import ConexionMysql.conexion;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import utilidades.tecladonumerico;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import jssc.SerialPortException;

/**
 *
 * @author Roberto Jimenez:
 * Robertchio@gmail.com
 */
public class ediciondepesadaguardada extends javax.swing.JFrame implements Runnable{
    private static final long serialVersionUID = 1L;
    private String fechayhoraactual = "";            // yyyy-MM-dd HH:mm:ss
    private String nombrerutatemporal = "";          // Nombre de la ruta para hacer las consultas e inserts mysql
    private String nombrearticulotemporal = "";      // Lo mismo que la linea de arriba
    private String idrutatemporal = "";              // Mas de lo mismo
    private String idarticulotemporal = "";          // Ajumm         
    private String diadehoy = "";                    // Por si lo quiero mostrar en la interfaz.
    private static int salio1entro2 = 0;       // colocamos 1 si sale el reparto y 2 si llega, es para guardar este digito en la base de datos.
    private static String haydatosenlafecha = "no";  // es para controlar cuando se pulsa guardar lote que hayan introducido una fecha de caducidad.
    private static String fechalotetemporal = "";
    private String idpesadatemporal = "";
  

    public void setIdpesadatemporal(String idpesadatemporal) {
        this.idpesadatemporal = idpesadatemporal;
    }

 
    /**
     *
     */
    public  ediciondepesadaguardada() {
        initComponents();
        Toolkit toolkit = getToolkit();
        Dimension size = toolkit.getScreenSize();
        setLocation(size.width/2 - getWidth()/2,
                size.height/2 - getHeight()/2);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH); 
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/icono_app.png")).getImage());         
    } 
    private void leerrutasajtable(){
                 try {
                  String consulta = "Select rutas.nombre from rutas";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                ResultSetMetaData meta = rs.getMetaData();
                int numerocolumnas = meta.getColumnCount();
                DefaultTableModel modelo = new DefaultTableModel();
                this.jTablerutas.setModel(modelo);
          for (int x=1; x<=numerocolumnas; x++){
             modelo.addColumn(meta.getColumnLabel(x));          
          }
          while (rs.next()){
                    Object [] fila = new Object [numerocolumnas];
                for(int y = 0; y<numerocolumnas; y++){
                    fila [y]= rs.getObject(y+1);
                    }
                 modelo.addRow(fila);
           }
           
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    private void leerarticulosjtable(){
            try {
                  String consulta = "Select articulos.nombre from articulos";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                ResultSetMetaData meta = rs.getMetaData();
                int numerocolumnas = meta.getColumnCount();
                DefaultTableModel modelo = new DefaultTableModel();
                this.jTablearticulos.setModel(modelo);
          for (int x=1; x<=numerocolumnas; x++){
             modelo.addColumn(meta.getColumnLabel(x));          
          }
          while (rs.next()){
                    Object [] fila = new Object [numerocolumnas];
                for(int y = 0; y<numerocolumnas; y++){
                    fila [y]= rs.getObject(y+1);
                    }
                 modelo.addRow(fila);
           }
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    
    private void conseguirrutaseleccionada(){
            int row = jTablerutas.getSelectedRow();
            String seleccion = (jTablerutas.getModel().getValueAt(row, 0).toString());
            nombrerutatemporal = seleccion;
            try {
                  String consulta = "Select rutas.idrutas from pesaje.rutas where rutas.nombre ='"+seleccion+"'";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                // lo siguiente es para pasar el unico registro obtenido del resultset a string pero hace falta siempre el label de la columna.
                while (rs.next()) {                    
            idrutatemporal = rs.getString("idrutas");  
                }
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    private void ConseguirIdArticuloSeleccionado(){
        
            int row = jTablearticulos.getSelectedRow();
            String seleccion = (jTablearticulos.getModel().getValueAt(row, 0).toString());
            nombrearticulotemporal = seleccion;
            try {
                ConexionMysql.conexion ca = new ConexionMysql.conexion();
                  String consulta = "Select articulos.idarticulos from pesaje.articulos where articulos.nombre = '"+seleccion+"'";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                // lo siguiente es para pasar el unico registro obtenido del resultset a string pero hace falta siempre el label de la columna.
                while (rs.next()) {                    
                           idarticulotemporal = rs.getString("idarticulos");
                }
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    private void ConseguirNombreArticulo(){       
            try {
                ConexionMysql.conexion ca = new ConexionMysql.conexion();
                  String consulta = "Select articulos.nombre from pesaje.articulos where articulos.idarticulos = '"+idarticulotemporal+"'";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                // lo siguiente es para pasar el unico registro obtenido del resultset a string pero hace falta siempre el label de la columna.
                while (rs.next()) {                    
                           nombrearticulotemporal = rs.getString("nombre");
                }
        } catch (ClassNotFoundException | SQLException e) {
        }
    }
    private void ConseguirNombreRuta(){       
            try {
                ConexionMysql.conexion ca = new ConexionMysql.conexion();
                  String consulta = "Select rutas.nombre from pesaje.rutas where rutas.idrutas = '"+idrutatemporal+"'";
                Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                ResultSet rs = stm.executeQuery(consulta);
                // lo siguiente es para pasar el unico registro obtenido del resultset a string pero hace falta siempre el label de la columna.
                while (rs.next()) {                    
                           nombrerutatemporal = rs.getString("nombre");
                }
        } catch (ClassNotFoundException | SQLException e) {
        }
    }

    private void conseguirdiadehoy(){
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, 0);
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                    String choy = format1.format(cal.getTime());
                    diadehoy = choy;
    }
    private void conseguirfechayhoraactual(){
                        Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, 0);
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    String ahoramismo = format1.format(cal.getTime());
                    fechayhoraactual = ahoramismo;    
    
    } 

    private void guardarpesadas(){
           if (nombrerutatemporal.isEmpty()){
               JOptionPane.showMessageDialog(null, "Seleccione una ruta");
           }else if(nombrearticulotemporal.isEmpty()){
               JOptionPane.showMessageDialog(null, "Seleccione un articulo");
           }else if("".equals(jTextFieldPesaEdicion.getText())){
               JOptionPane.showMessageDialog(null, "Introduzca el peso manualmente o pulse leer pesa");
           }else if("".equals(jTextFieldCajasEdicion.getText())){
               JOptionPane.showMessageDialog(null, "Introduzca las cajas que fueron utilizadas");
           }else if(!jRadioBtnSalir.isSelected() && !jRadioBtnEntrar.isSelected()){
               JOptionPane.showMessageDialog(null, "Seleccione si la pesada se realizó al Salir o al Entrar");
           }else{  
                       if(jRadioBtnSalir.isSelected()){
                          salio1entro2 = 1;
                       }else{
                          salio1entro2 = 2;
                       }
               try {
                    double pesodelpanelpesaParseDouble = Double.parseDouble(jTextFieldPesaEdicion.getText());
                    double cajasmetidasParseDouble = Double.parseDouble(jTextFieldCajasEdicion.getText());
                    double kilosencajas = cajasmetidasParseDouble * 2.7;
                    double kilossincajaaguardar = (pesodelpanelpesaParseDouble - kilosencajas);
                    conexion con = new ConexionMysql.conexion();
                    String insertar;
                           insertar = "update pesaje.pesadas set kilos = "+kilossincajaaguardar+", kilos_cajas = "+kilosencajas+", salieron1entraron2 = "
                                   +salio1entro2+", rutas_idrutas = "+idrutatemporal+", articulos_idarticulos = "+idarticulotemporal+" where idpesadas = "+idpesadatemporal+"";
                        con.actualizar(insertar);
                        JOptionPane.showMessageDialog(null, "Datos guardados");
                        this.dispose();
                    } catch (ClassNotFoundException | HeadlessException e) {
                        JOptionPane.showInternalMessageDialog(null, "Error al guardar los datos "+e);
                  }
            }
    }

    public  void IrMenuPrincipal(){  //dialogo en el que preguntamos si desea salir de la aplicación o no.
        Object [] opciones ={"Aceptar","Cancelar"};
        int eleccion = JOptionPane.showOptionDialog(rootPane,"¿En realidad desea regresar al menu principal?","Salir al menu principal",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE,null,opciones,"Aceptar");
        if (eleccion == JOptionPane.YES_OPTION)
        {
        Pesajevistas.eleccion aaa = new eleccion();
        aaa.setVisible(true);
        dispose();
        }else{
    }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btngroupsaliooentro = new javax.swing.ButtonGroup();
        btnguardarpesada = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTablearticulos = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTablerutas = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jButtonleerpesa = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnsalir = new javax.swing.JButton();
        jTextFieldPesaEdicion = new javax.swing.JTextField();
        jTextFieldCajasEdicion = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        lbidpesadas = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lbrutaanterior = new javax.swing.JLabel();
        lbarticuloanterior = new javax.swing.JLabel();
        lbpesoanterior = new javax.swing.JLabel();
        lbpesadoalsaliroentrar = new javax.swing.JLabel();
        lbkilosencajas = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jRadioBtnSalir = new javax.swing.JRadioButton();
        jRadioBtnEntrar = new javax.swing.JRadioButton();
        jLabel12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Editor de pesadas");
        setLocationByPlatform(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        btnguardarpesada.setBackground(new java.awt.Color(201, 109, 0));
        btnguardarpesada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/22x22/actions/document-edit.png"))); // NOI18N
        btnguardarpesada.setText("Guardar Peso");
        btnguardarpesada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnguardarpesadaActionPerformed(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Rutas");

        jTablearticulos.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jTablearticulos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Title 1"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTablearticulos.setColumnSelectionAllowed(true);
        jTablearticulos.setRowHeight(40);
        jTablearticulos.setRowMargin(2);
        jTablearticulos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTablearticulos.getTableHeader().setReorderingAllowed(false);
        jTablearticulos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTablearticulosMouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(jTablearticulos);
        jTablearticulos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jScrollPane5.setViewportView(jScrollPane3);

        jTablerutas.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jTablerutas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Title 1"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTablerutas.setColumnSelectionAllowed(true);
        jTablerutas.setRowHeight(40);
        jTablerutas.setRowMargin(2);
        jTablerutas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTablerutas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTablerutasMouseReleased(evt);
            }
        });
        jScrollPane4.setViewportView(jTablerutas);
        jTablerutas.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jScrollPane6.setViewportView(jScrollPane4);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Kg o Litros");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Articulo");

        jButtonleerpesa.setBackground(new java.awt.Color(201, 109, 0));
        jButtonleerpesa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pesa.png"))); // NOI18N
        jButtonleerpesa.setText("Leer pesa");
        jButtonleerpesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonleerpesaActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Numero de cajas");
        jLabel2.setAutoscrolls(true);

        jPanel1.setBackground(new java.awt.Color(201, 109, 0));

        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/22x22/actions/go-previous.png"))); // NOI18N
        btnsalir.setOpaque(false);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
        );

        jTextFieldPesaEdicion.setEditable(false);
        jTextFieldPesaEdicion.setBackground(new java.awt.Color(255, 255, 255));
        jTextFieldPesaEdicion.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jTextFieldPesaEdicion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldPesaEdicion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextFieldPesaEdicionMouseClicked(evt);
            }
        });

        jTextFieldCajasEdicion.setEditable(false);
        jTextFieldCajasEdicion.setBackground(new java.awt.Color(255, 255, 255));
        jTextFieldCajasEdicion.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jTextFieldCajasEdicion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldCajasEdicion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextFieldCajasEdicionMouseClicked(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel3.setText("Se está editando la pesada id :");

        lbidpesadas.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbidpesadas.setForeground(new java.awt.Color(201, 109, 0));
        lbidpesadas.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel6.setText("Ruta anterior :");

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel7.setText("Pesado al :");

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel8.setText("Articulo :");

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel9.setText("Peso :");

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel10.setText("Cajas :");

        lbrutaanterior.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbrutaanterior.setForeground(new java.awt.Color(201, 109, 0));
        lbrutaanterior.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        lbarticuloanterior.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbarticuloanterior.setForeground(new java.awt.Color(201, 109, 0));
        lbarticuloanterior.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        lbpesoanterior.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbpesoanterior.setForeground(new java.awt.Color(201, 109, 0));
        lbpesoanterior.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        lbpesadoalsaliroentrar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbpesadoalsaliroentrar.setForeground(new java.awt.Color(201, 109, 0));
        lbpesadoalsaliroentrar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        lbkilosencajas.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbkilosencajas.setForeground(new java.awt.Color(201, 109, 0));
        lbkilosencajas.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/editar.png"))); // NOI18N

        jRadioBtnSalir.setText("Salir");

        jRadioBtnEntrar.setText("Entrar");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jRadioBtnSalir)
                    .addComponent(jRadioBtnEntrar)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jRadioBtnSalir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jRadioBtnEntrar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel12.setText("Pesado al:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addGap(84, 84, 84)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnguardarpesada, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFieldCajasEdicion, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jButtonleerpesa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFieldPesaEdicion, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18))
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel7)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbarticuloanterior, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)
                    .addComponent(lbidpesadas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbpesadoalsaliroentrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbrutaanterior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbpesoanterior, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbkilosencajas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel3)
                                            .addComponent(lbidpesadas, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel6))
                                    .addComponent(lbrutaanterior, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8))
                            .addComponent(lbarticuloanterior, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9))
                    .addComponent(lbpesoanterior, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel10)
                    .addComponent(lbkilosencajas, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7)
                    .addComponent(lbpesadoalsaliroentrar, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextFieldPesaEdicion, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonleerpesa)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldCajasEdicion, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnguardarpesada)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        btnguardarpesada.getAccessibleContext().setAccessibleName("GuardarPeso");

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
           this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed
    private void btnguardarpesadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnguardarpesadaActionPerformed
        guardarpesadas();  
    }//GEN-LAST:event_btnguardarpesadaActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        btngroupsaliooentro.add(jRadioBtnSalir);
        btngroupsaliooentro.add(jRadioBtnEntrar);
        leerpesadaguardada();
        leerarticulosjtable();
        leerrutasajtable();
        ConseguirNombreArticulo();
        ConseguirNombreRuta();
        rellenarlabels();
        conseguirdiadehoy();
        conseguirfechayhoraactual();
        
    }//GEN-LAST:event_formWindowOpened
    private void leerpesadaguardada(){
                try {
                    String consulta;
                    consulta = "select * from pesadas where idpesadas = '"+idpesadatemporal+"'"; // group by 2"; // curdate es fecha actual.
                    Statement stm = ConexionMysql.conexion.setStatement(); //inicia la conexion
                    ResultSet rs = stm.executeQuery(consulta);
                if (rs.next()) {
                    idpesadatemporal = rs.getString("idpesadas");
                    
                    double kiloscajas = rs.getDouble("kilos_cajas");
                    double kilossincajas = rs.getDouble("kilos");
                    double restaurarpesooriginal = kilossincajas + kiloscajas;
//                    DecimalFormat df = new DecimalFormat("###.###");
//                    df.format(restaurarpesooriginal);
                    String abbb = String.valueOf(restaurarpesooriginal);
                    jTextFieldPesaEdicion.setText(abbb);
                    double aaaa = rs.getDouble("kilos_cajas");
                    double ababbb = aaaa / 2.7;
                    String abab = String.valueOf(ababbb);
                    jTextFieldCajasEdicion.setText(abab);
                    idrutatemporal = rs.getString("rutas_idrutas");
                    idarticulotemporal = rs.getString("articulos_idarticulos");
                    salio1entro2 = rs.getInt("salieron1entraron2");   
                }else{
                    JOptionPane.showMessageDialog(null, "No se encontraron resultados, ésto es algo muy raro...");
                }
        } catch (ClassNotFoundException | SQLException e) {
                        System.err.println("Error al conectar: "+e);
        }

    } 
    private void rellenarlabels(){
                    if (salio1entro2 == 1){
                       lbpesadoalsaliroentrar.setText("Salir");
                       jRadioBtnSalir.setSelected(true);
                    }else if(salio1entro2 == 2){
                        lbpesadoalsaliroentrar.setText("Entrar");
                        jRadioBtnEntrar.setSelected(true);
                    }else{
                        lbpesadoalsaliroentrar.setText("Error");
                    }
                    lbidpesadas.setText(idpesadatemporal);
                    lbrutaanterior.setText(nombrerutatemporal);
                    lbarticuloanterior.setText(nombrearticulotemporal);
                    lbpesoanterior.setText(jTextFieldPesaEdicion.getText());
                    lbkilosencajas.setText(jTextFieldCajasEdicion.getText());
    }
    
    private void jTablerutasMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablerutasMouseReleased
                    conseguirrutaseleccionada();
    }//GEN-LAST:event_jTablerutasMouseReleased
    private void jTablearticulosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablearticulosMouseReleased
            int row = jTablearticulos.getSelectedRow();
            String seleccion = (jTablearticulos.getModel().getValueAt(row, 0).toString());
            nombrearticulotemporal = seleccion;
            ConseguirIdArticuloSeleccionado();
    }//GEN-LAST:event_jTablearticulosMouseReleased

    private void jButtonleerpesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonleerpesaActionPerformed
             utilidades.lecturapuertoserie.setLeerpesasalida1entrada2(1);
        try {
            utilidades.lecturapuertoserie.leerpuerto();//
        } catch (SerialPortException ex) {
            Logger.getLogger(ediciondepesadaguardada.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }//GEN-LAST:event_jButtonleerpesaActionPerformed

    private void jTextFieldPesaEdicionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldPesaEdicionMouseClicked
               utilidades.tecladonumerico.setdedondefuellamadoelteclado(9); //llamo al tecladonumerico emergente y asigno un 9  y saber desde donde fue llamado.
               utilidades.tecladonumerico tec = new tecladonumerico();
               tec.setVisible(true);
    }//GEN-LAST:event_jTextFieldPesaEdicionMouseClicked

    private void jTextFieldCajasEdicionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldCajasEdicionMouseClicked
             utilidades.tecladonumerico.setdedondefuellamadoelteclado(10);// le decimos al teclado emergernte que ha sido llamado desde salida, cajas.
             utilidades.tecladonumerico tec = new tecladonumerico();
             tec.setVisible(true);
    }//GEN-LAST:event_jTextFieldCajasEdicionMouseClicked
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ediciondepesadaguardada.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        /* Create and display the form */
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new ediciondepesadaguardada().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btngroupsaliooentro;
    private javax.swing.JButton btnguardarpesada;
    private javax.swing.JButton btnsalir;
    private javax.swing.JButton jButtonleerpesa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JRadioButton jRadioBtnEntrar;
    private javax.swing.JRadioButton jRadioBtnSalir;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTablearticulos;
    private javax.swing.JTable jTablerutas;
    public static javax.swing.JTextField jTextFieldCajasEdicion;
    public static javax.swing.JTextField jTextFieldPesaEdicion;
    private javax.swing.JLabel lbarticuloanterior;
    private javax.swing.JLabel lbidpesadas;
    private javax.swing.JLabel lbkilosencajas;
    private javax.swing.JLabel lbpesadoalsaliroentrar;
    private javax.swing.JLabel lbpesoanterior;
    private javax.swing.JLabel lbrutaanterior;
    // End of variables declaration//GEN-END:variables

    @Override
    public void run() {
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    }

}
