/*
 * Copyright (C) 2015 rob
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ConexionMysql;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import Pesajevistas.eleccion;
import Pesajevistas.login;

/**
 *
 * @author robertchio@gmail.com
 */
public class conexion {
        
    public static Connection cn = null;   //todo estaba en privado y lo he cambiado 
    public static Statement st = null;
    public static PreparedStatement ps = null;
    public static ResultSet rs = null;
    
    public static final int CERRAR_TODO = 1;
    public static final int CERRAR_STATEMENT = 2;
    public static final int CERRAR_PREPAREDSTATEMENT = 3;
    public static final int CERRAR_ST_PS = 4;
    
    
    private static String host = "";
    private static String dataBase = "";
    private static String user = "";
    private static String pass = "";

    public static String getHost() {
        return host;
    }

    public static void setHost(String host) {
        conexion.host = host;
    }
    public static String getNombreBD() {
        return dataBase;
    }

    public static void setNombreBD(String nombreBD) {
        conexion.dataBase = nombreBD;
    }

    public static String getUsuario() {
        return user;
    }

    public static void setUsuario(String usuario) {
        conexion.user = usuario;
    }

    public static String getClave() {
        return pass;
    }

    public static void setClave(String clave) {
        conexion.pass = clave;
    }


    public static void conectar() throws ClassNotFoundException{ 
          try{
              if (cn==null){
                    Class.forName("com.mysql.jdbc.Driver");
                    String url = "jdbc:mysql://"+host+":3306/"+dataBase;
                    cn = DriverManager.getConnection( url, user, pass );
                 }
          }catch(ClassNotFoundException ex){
              JOptionPane.showMessageDialog(null, "Error Interno!", "Registro de Conexión falló", JOptionPane.ERROR_MESSAGE);
          }
          catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Acceso denegado!!", "Usuario NO Autorizado", JOptionPane.ERROR_MESSAGE);
          }
    }
  public static String login(String usua ,String contras) throws ClassNotFoundException{
      
            if ("".equals(usua) && "".equals(contras)) {
                JOptionPane.showMessageDialog(null, "Introduzca usuario y contraseña");
            }else{
            try{
                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://"+host+":3306/"+dataBase;
                cn = DriverManager.getConnection(url, user, pass);
               // cn = DriverManager.getConnection(url, usuario, clave);
                st = cn.createStatement();            
                rs = st.executeQuery("select username , password from usuarios where username='"+usua+"'and password='"+contras+"'");

                if(rs.next()){
                       String aa = "si";
                       return aa;
                     }
                else{
                       String aa = "no";
                       return aa;
                    }
            }catch(SQLException ex){}
        return null;
            }
        return null;
    } 

    public Connection consultar(String consulta) throws ClassNotFoundException{

            try{
                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://"+host+":3306/"+dataBase;
                cn = DriverManager.getConnection(url, user, pass);
                st = cn.createStatement();            
                st.executeUpdate(consulta); //  rs = st.executeQuery(consulta);
            }catch(SQLException ex){
                System.out.println(ex);
                JOptionPane.showMessageDialog( null, ex);
            }
        return null;
    } 
    public Connection actualizar(String Sentencia_actualizar) throws ClassNotFoundException{

            try{
                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://"+host+":3306/"+dataBase;
                cn = DriverManager.getConnection(url, user, pass);
                st = cn.createStatement();            
                st.executeUpdate(Sentencia_actualizar); //  rs = st.executeQuery(consulta);
            }catch(SQLException ex){
                System.out.println(ex);
                JOptionPane.showMessageDialog( null, ex);
            }
        return null;
    } 
  
    public static PreparedStatement preparedStatement(String sql) throws ClassNotFoundException
    {
        if(cn == null) {
            conectar();
        }
        try{
            System.out.println("preparestatement ejecutado desde conexion");
            ps = cn.prepareStatement(sql);
                        System.out.println(ps);
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "No se pudo ejecutar la consulta!!", "no se pudo obtener registros", JOptionPane.ERROR_MESSAGE);
            try{
                if(!ps.isClosed()) {
                    ps.close();
                }
            }catch(SQLException ex2){}
            
        }
        return ps;
    }
        public static Statement setStatement() throws ClassNotFoundException
    {
        if(cn == null) {
            conectar();
        }
        try{
            st = cn.createStatement();
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "No se pudo ejecutar la consulta!!", "no se pudo obtener registros", JOptionPane.ERROR_MESSAGE);
            try{
                if(!st.isClosed()) {
                    st.close();
                }
            }catch(SQLException ex2){}
            
        }
        
        return st;
    }
    public static void cerrarTodo(int tipoCierre)
    {
        try{
            switch(tipoCierre)
            {
                case CERRAR_TODO:            
                    cerrarConexion();
                    cerrarStatement();
                    cerrarPreparedStatement();
                    break;
                case CERRAR_STATEMENT:
                    cerrarStatement();
                    break;
                case CERRAR_PREPAREDSTATEMENT:
                    cerrarPreparedStatement();
                    break;
                case CERRAR_ST_PS:
                    cerrarStatement();
                    cerrarPreparedStatement();
                    break;
                default:System.out.println();
            }
        }catch(SQLException ex){}
    }
        private static void cerrarConexion()throws SQLException
    {
        if(cn !=null)
        {
            if(!cn.isClosed())
            {
                cn.close();
            }
        }
    }
    private static void cerrarStatement()throws SQLException
    {
        if(st != null)
        {
            if(!st.isClosed()) {
                st.close();
            }
        }
    }
    
    private static void cerrarPreparedStatement()throws SQLException
    {
       if(ps != null)
        {
            if(!ps.isClosed()) {
                ps.close();
            }
        }
    }
    
    
    public Object[][] consultaarrayRob (String consultatoarray) throws ClassNotFoundException{
        Object[][] filas = null;
        ResultSet rs = null;
                        if(cn == null) {
                    conectar();
                         }
                try {
                            st = cn.createStatement();
                            rs = st.executeQuery(consultatoarray);
                            int numFilas = 0;
                            int numColumnas = rs.getMetaData().getColumnCount();
                        
                        while (rs.next()) {
                             numFilas++;                             
                            }
                             rs.beforeFirst();
                             int contadorTmp = 0;                            
                        filas = new Object[numFilas][numColumnas];
                        while (rs.next()){
                                filas[contadorTmp][0] = rs.getInt(1);
                                filas[contadorTmp][1] = rs.getInt(2);
                                filas[contadorTmp][2] = rs.getInt(3);
                                filas[contadorTmp][3] = rs.getInt(4);
                            }
                            st.close();
                            rs.close();
                            //return;
        } catch (SQLException ex) {}
                    return filas;
    }          


}       
    



    



