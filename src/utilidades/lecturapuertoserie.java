/*
 * Copyright (C) 2015 robertchio@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package utilidades;
/**
 *
 * @author Roberto Jimenez:
 * Robertchio@gmail.com
 */
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import jssc.*;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;


public class lecturapuertoserie implements Runnable{
     private static SerialPort serialPort; 
     private static int leerpesasalida1entrada2 = 0;    // poner a 0 esto-------------------------------

    public static void setLeerpesasalida1entrada2(int leerpesasalida1entrada2) {
        lecturapuertoserie.leerpesasalida1entrada2 = leerpesasalida1entrada2;
    }
        
    public static void puertosdisponibles(){
    // getting serial ports list into the array
        String[] portNames = SerialPortList.getPortNames();
        if (portNames.length == 0) {
            JOptionPane.showMessageDialog(null, "No dispone de puertos serie libres");
        }else{
            for (String portName : portNames) {
            }
            JOptionPane.showMessageDialog(null, portNames);
        }    
    }
    public static void leerpuerto()throws SerialPortException {  
         serialPort = new SerialPort(configuracion.getpuertoSerieNombre()); 
        try {
            serialPort.openPort();//Open port
            // Desabilito RTS y DTR porque solo usaremos RX, TX y GND.
            serialPort.setRTS(false); 
            serialPort.setDTR(false);
            serialPort.setParams(9600, 8, 1, 0);//Set params
         //   int mask = SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR;//Preparado de la mascara a enviar con los datos para el uso de DSR y CTS.
         //   serialPort.setEventsMask(mask);//Set mask
            serialPort.addEventListener(new SerialPortReader());//Add SerialPortEventListener
         // serialPort.writeString("P");   //Write data to port  Se le envía a la pesa para que imprima el peso pero esta pesa no sirve.
         //  serialPort.closePort();

        }catch (SerialPortException ext) {
              if (serialPort.isOpened()) {
                    serialPort.removeEventListener();
                    serialPort.closePort(); //Cierro el puerto si obtenemos datos que abri desde leerpuerto 
                    System.out.println("se cerro el puerto en leerpuerto: "+ext);
                }

            JOptionPane.showMessageDialog(null, "No se leyeron datos desde el puerto serie, compruebe el cableado y la configuración de la pesa.",
                                                "ERROR AL LEER EL PUERTO SERIE", JOptionPane.WARNING_MESSAGE);
        } 
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
      private static class SerialPortReader implements SerialPortEventListener {

        @Override
    public void serialEvent(SerialPortEvent event) {

            if(event.isRXCHAR() && event.getEventValue() > 0){  //If data is available
            try { 
            for(int i = 1 ; i <= 1000; i ++){ 
                //byte[] almohadilla = serialPort.readBytes(1);
                String almohadilla = serialPort.readString(1); // leo digito a digito para encontrar la  puta #
            if("#".equals(almohadilla)){
                 // byte[] buffer = serialPort.readBytes(10);//Read 10 bytes from serial port
            String str = serialPort.readString(10);
            try{
            String substring = str.substring(4,9); // corto la cadena solo para conseguir el peso.
            double pesodoble = Double.parseDouble(substring);
            double p = pesodoble / 10; // lo divido entre 10 para conseguir los kilos exactos. antes estaba en int.
            String strpeso = Double.toString(p);
            if (leerpesasalida1entrada2==1){
                Pesajevistas.salida.jTextFieldPesaSalida.setText(strpeso);
            }else if (leerpesasalida1entrada2==2) {
                Pesajevistas.llegada.jTextFieldPesallegada.setText(strpeso);
                }
            }catch(NumberFormatException exep){
 
                JOptionPane.showMessageDialog(null,"Error al leer la pesa,vuelva a pulsar: Leer pesa\n"+ exep);  
            }  
                i = 1000;    // salgo del for porque ya encontramos almohadilla #  
                serialPort.closePort();  // Cierro el puerto si hay un error al leer o convertir los datos.  
            }   /// if fin de la busqueda de la almohadilla          
            }   /// Cierre del for 
                if (serialPort.isOpened()) {

                    serialPort.removeEventListener();
                    serialPort.closePort(); //Cierro el puerto si obtenemos datos que abri desde leerpuerto 
                    //
                }           
//---------------------------------------------------------------------------------------------------------------------
            }catch (SerialPortException ex) {  
                System.out.println("Error al leer en el puerto serie: "+ex);
                JOptionPane.showInternalMessageDialog(null, ex);
            }
    }else if(event.isCTS()){//If CTS line has changed state
                if(event.getEventValue() == 1){//If line is ON
                    System.out.println("CTS - ON");
                }
                else {
                    System.out.println("CTS - OFF");
                }
    }else if(event.isDSR()){///If DSR line has changed state
                if(event.getEventValue() == 1){//If line is ON
                    System.out.println("DSR - ON");
                }else {
                    System.out.println("DSR - OFF");
                }
    }else{
                JOptionPane.showMessageDialog(null, "No hay datos desde el puerto serie");
                try {
                if (serialPort.isOpened()) {
                    serialPort.removeEventListener();
                    serialPort.closePort(); //Cierro el puerto si obtenemos datos que abri desde leerpuerto 
                }

                } catch (SerialPortException ex) {
                    Logger.getLogger(lecturapuertoserie.class.getName()).log(Level.SEVERE, null, ex);
                }
 
    }
 
        } 
 
    } 

}  

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////77
//                        public static void leerenelpuerto() {
//        SerialPort serialPort = new SerialPort("COM7");
//        try {
//            serialPort.openPort();//Open serial port
//            serialPort.setParams(9600, 8, 1, 0);//Set params.
//            byte[] buffer = serialPort.readBytes(10);//Read 10 bytes from serial port
//            String str = new String(buffer);
//                      //  double p = Double.parseDouble(str);
//                        if(leerpesasalida1entrada2 == 1){           //llamado desde salida del reparto.
//                            System.out.print(str);
//                          //  salida.setlecturapesa(p);
//                            salida.getWindows();
//                        }else if (leerpesasalida1entrada2 == 2){    // llamado desde entrada del reparto
//                    //        llegada.setlecturapesa(p);
//                        }else{
//                        JOptionPane.showMessageDialog(null, "No se ha llamado la funcion leer pesa desde la salida o llegada del reparto.");
//                        }
//                        System.out.println(str);
//                        serialPort.closePort();
//        }
//        catch (SerialPortException ex) {
//            System.out.println(ex);
//        }
//    }
