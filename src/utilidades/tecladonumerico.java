/*
 * Copyright (C) 2015 robertchio@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package utilidades;
/**
 *
 * @author Roberto Jimenez:
 * Robertchio@gmail.com
 */
import Pesajevistas.ediciondepesadaguardada;
import java.awt.HeadlessException;
import javax.swing.JOptionPane;
import static org.codehaus.groovy.runtime.DefaultGroovyMethods.isInteger;
import Pesajevistas.llegada;
import Pesajevistas.salida;
import javax.swing.ImageIcon;
public class tecladonumerico extends javax.swing.JFrame {
    private static final long serialVersionUID = 1L;

    //Definimos las variables globales
    private String strAnterior="";
    private String strOperador="";
    private static int dedondefuellamadoelteclado = 0;  // guardo 1,2,3,4 (salida cantidad1lotes2lecturapesa3,cajas4) y de (entrada cantidad5lotes6lecturapesa7,cajas8).
    
    public static void setdedondefuellamadoelteclado(int dedondesellamaelteclado) {
        tecladonumerico.dedondefuellamadoelteclado = dedondesellamaelteclado;
    }

    public tecladonumerico() {
        initComponents();
        setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/icono_app.png")).getImage());        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        btnSiete = new javax.swing.JButton();
        btnOcho = new javax.swing.JButton();
        btnCuatro = new javax.swing.JButton();
        btnNueve = new javax.swing.JButton();
        btnCinco = new javax.swing.JButton();
        btnPunto = new javax.swing.JButton();
        btnUno = new javax.swing.JButton();
        btnTres = new javax.swing.JButton();
        btnCero = new javax.swing.JButton();
        btnSeis = new javax.swing.JButton();
        btnDos = new javax.swing.JButton();
        btnIgual = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        txtlcd = new javax.swing.JTextField();
        btnaceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Teclado emergente");
        setAlwaysOnTop(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        btnSiete.setText("7");
        btnSiete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSieteActionPerformed(evt);
            }
        });

        btnOcho.setText("8");
        btnOcho.setMaximumSize(new java.awt.Dimension(39, 35));
        btnOcho.setName(""); // NOI18N
        btnOcho.setPreferredSize(new java.awt.Dimension(39, 35));
        btnOcho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOchoActionPerformed(evt);
            }
        });

        btnCuatro.setText("4");
        btnCuatro.setPreferredSize(new java.awt.Dimension(39, 35));
        btnCuatro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCuatroActionPerformed(evt);
            }
        });

        btnNueve.setText("9");
        btnNueve.setPreferredSize(new java.awt.Dimension(39, 35));
        btnNueve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNueveActionPerformed(evt);
            }
        });

        btnCinco.setText("5");
        btnCinco.setPreferredSize(new java.awt.Dimension(39, 35));
        btnCinco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCincoActionPerformed(evt);
            }
        });

        btnPunto.setText(".");
        btnPunto.setPreferredSize(new java.awt.Dimension(39, 35));
        btnPunto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPuntoActionPerformed(evt);
            }
        });

        btnUno.setText("1");
        btnUno.setPreferredSize(new java.awt.Dimension(39, 35));
        btnUno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUnoActionPerformed(evt);
            }
        });

        btnTres.setText("3");
        btnTres.setName(""); // NOI18N
        btnTres.setPreferredSize(new java.awt.Dimension(39, 35));
        btnTres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTresActionPerformed(evt);
            }
        });

        btnCero.setText("0");
        btnCero.setPreferredSize(new java.awt.Dimension(39, 35));
        btnCero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCeroActionPerformed(evt);
            }
        });

        btnSeis.setText("6");
        btnSeis.setPreferredSize(new java.awt.Dimension(39, 35));
        btnSeis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeisActionPerformed(evt);
            }
        });

        btnDos.setText("2");
        btnDos.setPreferredSize(new java.awt.Dimension(39, 35));
        btnDos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDosActionPerformed(evt);
            }
        });

        btnIgual.setText("=");
        btnIgual.setPreferredSize(new java.awt.Dimension(39, 35));
        btnIgual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIgualActionPerformed(evt);
            }
        });

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/22x22/actions/draw-eraser.png"))); // NOI18N
        btnDelete.setRequestFocusEnabled(false);
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        txtlcd.setEditable(false);
        txtlcd.setBackground(new java.awt.Color(255, 255, 255));
        txtlcd.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/22x22/actions/dialog-ok-apply.png"))); // NOI18N
        btnaceptar.setToolTipText("Permite reducir la cantidad de decimales a un valor aproximado");
        btnaceptar.setPreferredSize(new java.awt.Dimension(51, 35));
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnSiete, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnCuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnUno, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnCero, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnOcho, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnCinco, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnDos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(btnPunto, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnIgual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnTres, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
                                    .addComponent(btnSeis, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
                                    .addComponent(btnNueve, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnaceptar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(0, 22, Short.MAX_VALUE))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtlcd, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtlcd, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnSiete, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnUno, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCero, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnOcho, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnCinco, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnDos, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnNueve, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnSeis, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnTres, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnPunto, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnIgual, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOchoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOchoActionPerformed
        //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador))
        {
            Limpiar();
            strAnterior=txtlcd.getText();
            txtlcd.setText("");
            txtlcd.setText(strAnterior+"8");
        }
        else
        {
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"8");
        }
    }//GEN-LAST:event_btnOchoActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        txtlcd.setText("");
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnUnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUnoActionPerformed
        //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador))
        {
            Limpiar();
            strAnterior=txtlcd.getText();
            txtlcd.setText("");
            txtlcd.setText(strAnterior+"1");
        }
        else
        {
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"1");
        }
    }//GEN-LAST:event_btnUnoActionPerformed

    private void btnDosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDosActionPerformed
        //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador))
        {
            Limpiar();
            strAnterior=txtlcd.getText();
            txtlcd.setText("");
            txtlcd.setText(strAnterior+"2");
           
        }
        else
        {
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"2");
        }
    }//GEN-LAST:event_btnDosActionPerformed

    private void btnTresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTresActionPerformed
        //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador))
        {
            Limpiar();
            txtlcd.setText("");
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"3");
            
        }
        else
        {
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"3");
        }
    }//GEN-LAST:event_btnTresActionPerformed
    private void btnCuatroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCuatroActionPerformed
        //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador))  //  if (strOperador=="=")
        {
            Limpiar();
            txtlcd.setText("");
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"4");
            
        }
        else
        {
            
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"4");
        }
    }//GEN-LAST:event_btnCuatroActionPerformed

    private void btnCincoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCincoActionPerformed
        //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador))//Si el operador es igual  if (strOperador=="=")
        {
            Limpiar();
            txtlcd.setText("");//
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"5");
            
        }
        else
        {
            
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"5");
        }
    }//GEN-LAST:event_btnCincoActionPerformed

    private void btnSeisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeisActionPerformed
        //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador)) // if (strOperador=="=")
        {
            Limpiar();
            txtlcd.setText("");
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"6");
           
        }
        else
        {
           
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"6");
        }
    }//GEN-LAST:event_btnSeisActionPerformed

    private void btnSieteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSieteActionPerformed
        //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador)) // if (strOperador=="=")
        {
            Limpiar();
            txtlcd.setText("");
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"7");
           
        }
        else
        {
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"7");
        }
    }//GEN-LAST:event_btnSieteActionPerformed

    private void btnNueveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNueveActionPerformed
       //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador)) // if (strOperador=="=")
        {
            Limpiar();
            txtlcd.setText("");
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"9");
            
        }
        else
        {
            
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"9");
        }
    }//GEN-LAST:event_btnNueveActionPerformed

    private void btnCeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCeroActionPerformed
        //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador))  // if (strOperador=="=")
        {
            Limpiar();
            txtlcd.setText("");
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"0");
           
        }
        else
        {
            
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+"0");
        }
    }//GEN-LAST:event_btnCeroActionPerformed

    private void btnPuntoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPuntoActionPerformed
        //Primero se verifica si el ultimo valor de la variable strOperador
        //correspondio al signo Igual, de ser asi se ejecuta lo contenido 
        //en el IF, de lo contrario, lo contenido en el Else
        if ("=".equals(strOperador)) // if (strOperador=="=")
        {
            Limpiar();
            txtlcd.setText("");
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+".");
            
        }
        else
        {
            
            strAnterior=txtlcd.getText();
            txtlcd.setText(strAnterior+".");
        }
    }//GEN-LAST:event_btnPuntoActionPerformed

    private void btnIgualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIgualActionPerformed


    }//GEN-LAST:event_btnIgualActionPerformed

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed

        int c = dedondefuellamadoelteclado;
        switch (c) {
  case 1:           //Llamado desde salida jTextFieldcantidad
                    salida.jTextFieldCantidadSalida.setText(txtlcd.getText());
                    txtlcd.setText("");
                    dispose();
        break;
  case 2:           // Llamado desde salida lote
                    salida.jTextFieldLoteSalida.setText(txtlcd.getText());
                    txtlcd.setText("");
                    dispose(); 
        break;
  case 3:           // el teclado ha sido llamado desde la lectura de pesa en salida, por lo tanto escribire ahi el valor.
                    salida.jTextFieldPesaSalida.setText(txtlcd.getText());
                    txtlcd.setText("");
                    dispose(); 
        break;
  case 4:           // Lamado desde salidas jtextfielcajas
                    try {
                        boolean isIntegero = isInteger(txtlcd.getText());
                        if(isIntegero){
                        int numerointParseInt= Integer.parseInt(txtlcd.getText());
                        salida.jTextFieldCajasSalida.setText(String.valueOf(numerointParseInt));
                        txtlcd.setText("");
                        dispose(); 
                        }else{
                               JOptionPane.showMessageDialog(null, "Debe ser un numero entero");
                               txtlcd.setText("");
                               txtlcd.setFocusable(true);
                                }
                        }catch (NumberFormatException | HeadlessException er) {
                               JOptionPane.showMessageDialog(null, "Error al convertir los datos introducidos");
                               dispose();
                        }                    
        break;
  case 5:           //Llamado desde llegada jTextFieldcantidad
                    llegada.jTextFielCantidadLlegada.setText(txtlcd.getText());
                    txtlcd.setText("");
                    dispose(); 
        break;
  case 6:           // Llamado desde llegada lote
                    llegada.jTextFielLoteLlegada.setText(txtlcd.getText());
                    txtlcd.setText("");
                    dispose();
        break; 
  case 7:           // el teclado ha sido llamado desde la lectura de pesa en llegadas, por lo tanto escribire ahi el valor.
                    llegada.jTextFieldPesallegada.setText(txtlcd.getText());
                    txtlcd.setText("");
                    dispose(); 
        break;
  case 8:           // Lamado desde llegadas jtextfielcajas
                try {
                    boolean isIntegero = isInteger(txtlcd.getText());
                    if(isIntegero){
                    int numerointParseInt= Integer.parseInt(txtlcd.getText());
                    llegada.jTextFielCajasllegadas.setText(String.valueOf(numerointParseInt));
                    txtlcd.setText("");
                    dispose(); 
                    }else{
                           JOptionPane.showMessageDialog(null, "Debe ser un numero entero");
                           txtlcd.setText("");
                           txtlcd.setFocusable(true);
                            }
                    }catch (NumberFormatException | HeadlessException er) {
                           JOptionPane.showMessageDialog(null, "Error al convertir los datos introducidos");
                           dispose();
                    } 
  case 9: // le decimos al teclado emergernte que ha sido llamado edision pesadas jtextfiel.
               ediciondepesadaguardada.jTextFieldPesaEdicion.setText(txtlcd.getText());
               txtlcd.setText("");
                    dispose(); 
        break;
    case 10: // le decimos al teclado emergernte que ha sido llamado edision pesadas desde cajas textbox.
               try {
                    boolean isIntegero = isInteger(txtlcd.getText());
                    if(isIntegero){
                    int numerointParseInt= Integer.parseInt(txtlcd.getText());
                    ediciondepesadaguardada.jTextFieldCajasEdicion.setText(String.valueOf(numerointParseInt));
                    txtlcd.setText("");
                    dispose(); 
                    }else{
                           JOptionPane.showMessageDialog(null, "Debe ser un numero entero");
                           
                           txtlcd.setText("");
                           txtlcd.setFocusable(true);
                            }
                    }catch (NumberFormatException | HeadlessException er) {
                           JOptionPane.showMessageDialog(null, "Error al convertir los datos introducidos");
                           dispose();
                    } 
        break;
  default:
        JOptionPane.showMessageDialog(null, "Se ha llamado al teclado sin pulsar los eventos: unidades, lote o lectura pesa"); // por poner algo
        break;
}

    }//GEN-LAST:event_btnaceptarActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        dispose();
    }//GEN-LAST:event_formWindowClosing

    //Método para inicializar las Variables 
    private void Limpiar()
    {
        strOperador="";
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(tecladonumerico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(() -> {
//            new tecladonumerico().setVisible(true);
//        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCero;
    private javax.swing.JButton btnCinco;
    private javax.swing.JButton btnCuatro;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDos;
    private javax.swing.JButton btnIgual;
    private javax.swing.JButton btnNueve;
    private javax.swing.JButton btnOcho;
    private javax.swing.JButton btnPunto;
    private javax.swing.JButton btnSeis;
    private javax.swing.JButton btnSiete;
    private javax.swing.JButton btnTres;
    private javax.swing.JButton btnUno;
    private javax.swing.JButton btnaceptar;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtlcd;
    // End of variables declaration//GEN-END:variables
}
