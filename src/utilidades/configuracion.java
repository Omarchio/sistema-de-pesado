/*
 * Copyright (C) 2015 rob
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package utilidades;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import javax.swing.JOptionPane;


/**
 *
 * @author rob
 */
 public class configuracion {
  private static String puertoserienombre = "";
 
    public static void leertodaslasconfiguracionesdeinicio() {
     leernombrepuertoserieguardado();
     LeerdarConfiguracionDeMysqlalInicio();
    }
    public static String getpuertoSerieNombre() {
        return puertoserienombre;
    }    
    public static void leernombrepuertoserieguardado() {
            Properties propiedades = new Properties();
            InputStream entrada = null;

    try {
            entrada = new FileInputStream("configuracion.properties");
            propiedades.load(entrada);
            puertoserienombre = propiedades.getProperty("puertoserie");
    } catch (IOException io) {
        JOptionPane.showMessageDialog(null, "No existe el fichero de configuración. \n"+
                                            "Se creará un fichero nuevo...\n\n"+
                                            "Abra la apicación de nuevo.");
        craearchivodeconfiguracionpordefecto();
        System.exit(0);
    } finally {
        if (entrada != null) {
            try {
                entrada.close();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }

    } 

  }
    public static void LeerdarConfiguracionDeMysqlalInicio(){
           Properties propiedades = new Properties();
            InputStream entrada = null;

    try {
            entrada = new FileInputStream("configuracion.properties");
            propiedades.load(entrada);
            ConexionMysql.conexion.setHost(propiedades.getProperty("host"));
            ConexionMysql.conexion.setNombreBD(propiedades.getProperty("database"));
            ConexionMysql.conexion.setUsuario(propiedades.getProperty("usermysql"));
            ConexionMysql.conexion.setClave(propiedades.getProperty("passmysql"));

    } catch (IOException io) {
        JOptionPane.showMessageDialog(null, "No existe el fichero de configuración. \n"+
                                            "Se creará un fichero nuevo...\n\n"+
                                            "Abra la apicación de nuevo.");
        craearchivodeconfiguracionpordefecto();
        System.exit(0);
    } finally {
        if (entrada != null) {
            try {
                entrada.close();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }

    }
}
    public static void guardarnombredepuertoserie(String datoaguardar) throws FileNotFoundException {
        Properties propiedades = new Properties();
        String propsFileName = "configuracion.properties";
        try {
            try ( // Leemos el archivo existente
                FileInputStream configStream = new FileInputStream(propsFileName)) {
                propiedades.load(configStream);
                configStream.close();
            }  
          // asignamos los valores a las propiedades
          propiedades.setProperty("puertoserie", datoaguardar);
          //propiedades.setProperty("clave", "123456");
        // guardamos el archivo de propiedades en la carpeta de aplicación
          FileOutputStream salida = new FileOutputStream(propsFileName);
          propiedades.store(salida, propsFileName);
    } catch (IOException io) {
        JOptionPane.showInternalMessageDialog(null, io);
    } 
  } 
    public static void craearchivodeconfiguracionpordefecto() {
                Properties propiedades = new Properties();
                OutputStream salida = null;
            try {
                salida = new FileOutputStream("configuracion.properties");
                // asignamos los valores a las propiedades
                propiedades.setProperty("puertoserie", "COM1");
                propiedades.setProperty("host", "localhost");
                propiedades.setProperty("database", "nombrebasededatos");
                propiedades.setProperty("usermysql", "usuariodb");
                propiedades.setProperty("passmysql", "passdb");
                // guardamos el archivo de propiedades en la carpeta de aplicación
                propiedades.store(salida, null);
            } catch (IOException io) {
            } finally {
                if (salida != null) {
                    try {
                        salida.close();
                    } catch (IOException e) {
                    }
                }
          }     
      }  
    public static void GuardarConfiguracionMysql(String[] args) {
        Properties propiedades = new Properties();
        String propsFileName = "configuracion.properties";
        try {
            try ( // Leemos el archivo existente
                FileInputStream configStream = new FileInputStream(propsFileName)) {
                propiedades.load(configStream);
                configStream.close();
            }  
        // asignamos los valores a las propiedades
            propiedades.setProperty("host", args[0]);
            propiedades.setProperty("database", args[1]);
            propiedades.setProperty("usermysql", args[2]);
            propiedades.setProperty("passmysql", args[3]);
        // guardamos el archivo de propiedades en la carpeta de aplicación
          FileOutputStream salida = new FileOutputStream(propsFileName);
          propiedades.store(salida, propsFileName);
    } catch (IOException io) {
        JOptionPane.showInternalMessageDialog(null, io);
    } 
  }
 }        
